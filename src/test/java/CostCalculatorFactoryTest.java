import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import Shipment.IncrementCalculator;
import Shipment.CostCalculatorFactory;
import Shipment.GrounIncrementCalculator;
import Shipment.SameDayIncrementCalculator;
import Shipment.ShipmentType;
import Shipment.ThreeDayIncrementCostCalculator;

public class CostCalculatorFactoryTest {

	
	CostCalculatorFactory costCalculatorFactory;
	
	@Before
	public void setup(){
		costCalculatorFactory = new CostCalculatorFactory();
	}
	@Test
	public void testReturnCostCalculator() {
		IncrementCalculator groundCostCalculator = costCalculatorFactory.getCostCalculator(ShipmentType.GROUND);
		IncrementCalculator sameDayCostCalculator = costCalculatorFactory.getCostCalculator(ShipmentType.SAME_DAY);
		IncrementCalculator threeDayCostCalculator = costCalculatorFactory.getCostCalculator(ShipmentType.THREE_DAY);
	
		assertTrue(groundCostCalculator instanceof GrounIncrementCalculator);
		assertTrue(sameDayCostCalculator instanceof SameDayIncrementCalculator);
		assertTrue(threeDayCostCalculator instanceof ThreeDayIncrementCostCalculator);
	
	}
	
}
