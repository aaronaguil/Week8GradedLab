import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import Shipment.ShipmentType;
import core.DownloadableBook;
import core.HardCoverBook;
import core.TaxCalculator;

@RunWith(MockitoJUnitRunner.class)
public class DownloadableBookTest {
	DownloadableBook book;
	
	
	@Mock
	TaxCalculator zipLookUpAdapter;
	
	@Before
	public void setUp(){
		
		when(zipLookUpAdapter.lookUpZip(75087L)).thenReturn(8.25);
		book = new DownloadableBook("2345", 10.00, 75087L, zipLookUpAdapter);
	}
	
	@Test
	public void test() {
		assertEquals(11, book.getCost(), 0.01);
	}

}
