import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import Shipment.ShipmentType;
import core.HardCoverBook;
import core.TaxCalculator;

@RunWith(MockitoJUnitRunner.class)
public class HardCoverBookTest {

	HardCoverBook book;
	
	
	@Mock
	TaxCalculator zipLookUpAdapter;
	
	@Before
	public void setUp(){
		
		when(zipLookUpAdapter.lookUpZip(75087L)).thenReturn(8.25);
		book = new HardCoverBook("2388", 20.00, 75087L, ShipmentType.GROUND, zipLookUpAdapter);
	}
	
	@Test
	public void test() {
		assertEquals(23, book.getCost(), 0.01);
	}

}
