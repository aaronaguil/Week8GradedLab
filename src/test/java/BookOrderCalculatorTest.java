import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bobswarehouse.BookLookUpService;

import Shipment.ShipmentType;
import core.BookOrder;
import core.BookOrderCalculator;
import core.DownloadableBook;
import core.HardCoverBook;
import core.Warehouse;
import core.TaxCalculator;


@RunWith(MockitoJUnitRunner.class)
public class BookOrderCalculatorTest {

	private BookOrderCalculator bookOrderCalculator;
	private BookOrder bookOrder;
	
	@Mock
	TaxCalculator taxCalculator;
	
	private Warehouse warehouse;
	private BookLookUpService bookLookUpService;
	
	
	@Before
	public void setup(){
		BookOrder bookOrder = new BookOrder();
		bookLookUpService = new BookLookUpService();
		warehouse = new Warehouse(bookLookUpService);
		
		when(taxCalculator.lookUpZip(75087L)).thenReturn(8.25);
		when(taxCalculator.lookUpZip(30080L)).thenReturn(6.0);
		when(taxCalculator.lookUpZip(61761L)).thenReturn(8.75);
		when(taxCalculator.lookUpZip(85001)).thenReturn(8.60);
		
		DownloadableBook bookOne = new DownloadableBook("2345", 10.00, 75087L, taxCalculator);
		HardCoverBook bookTwo = new HardCoverBook("2388", 20.00, 75087L, ShipmentType.GROUND, taxCalculator);
		DownloadableBook bookThree = new DownloadableBook("1399", 15.00, 30080L, taxCalculator);
		DownloadableBook bookFour = new DownloadableBook("2222", 12.00, 85001L, taxCalculator);
		HardCoverBook bookFive = new HardCoverBook("1995", 10.00, 30080L, ShipmentType.SAME_DAY, taxCalculator);
		HardCoverBook bookSix = new HardCoverBook("2345", 20.00, 61761L, ShipmentType.THREE_DAY, taxCalculator);
		HardCoverBook bookSeven = new HardCoverBook("2337", 15.00, 30080L, ShipmentType.SAME_DAY, taxCalculator);
		
		bookOrder.addBook(bookOne);
		bookOrder.addBook(bookTwo);
		bookOrder.addBook(bookThree);
		bookOrder.addBook(bookFour);
		bookOrder.addBook(bookFive);
		bookOrder.addBook(bookSix);
		bookOrder.addBook(bookSeven);
		
		
		bookOrderCalculator = new BookOrderCalculator(bookOrder, warehouse);
		
		
	}
	
	
	@Test
	public void checkOrderCost(){
		assertEquals(91, bookOrderCalculator.calculateOrderCost(), .001);
	}
	
	
	

}
