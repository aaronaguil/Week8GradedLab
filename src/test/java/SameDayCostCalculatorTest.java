import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Shipment.SameDayIncrementCalculator;

public class SameDayCostCalculatorTest {

	private SameDayIncrementCalculator sameDayCostCalculator;

	@Before
	public void setup() {
		sameDayCostCalculator = new SameDayIncrementCalculator();
	}

	@Test
	public void checkCalculateCost() {
		double basePrice = 10.00;
		assertEquals(2.00, sameDayCostCalculator.calculateCost(basePrice), .001);
	}

}
