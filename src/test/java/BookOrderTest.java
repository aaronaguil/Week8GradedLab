import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import Shipment.ShipmentType;
import core.BookOrder;
import core.DownloadableBook;
import core.HardCoverBook;
import core.TaxCalculator;

@RunWith(MockitoJUnitRunner.class)
public class BookOrderTest {

	private BookOrder bookOrder;
	
	@Mock
	TaxCalculator taxCalculator;
	
	
	
	@Before
	public void setup(){
		bookOrder = new BookOrder();
		
		DownloadableBook bookOne = new DownloadableBook("2345", 10.00, 75087L, taxCalculator);
		HardCoverBook bookTwo = new HardCoverBook("2388", 20.00, 75087L, ShipmentType.GROUND, taxCalculator);
		DownloadableBook bookThree = new DownloadableBook("1399", 15.00, 30080L, taxCalculator);
		DownloadableBook bookFour = new DownloadableBook("2222", 12.00, 85001L, taxCalculator);
		HardCoverBook bookFive = new HardCoverBook("1995", 10.00, 30080L, ShipmentType.SAME_DAY, taxCalculator);
		HardCoverBook bookSix = new HardCoverBook("2345", 20.00, 61761L, ShipmentType.THREE_DAY, taxCalculator);
		HardCoverBook bookSeven = new HardCoverBook("2337", 15.00, 30080L, ShipmentType.SAME_DAY, taxCalculator);
		
		bookOrder.addBook(bookOne);
		bookOrder.addBook(bookTwo);
		bookOrder.addBook(bookThree);
		bookOrder.addBook(bookFour);
		bookOrder.addBook(bookFive);
		bookOrder.addBook(bookSix);
		bookOrder.addBook(bookSeven);
		
		
		
		
	}
	
	
	@Test
	public void checkOrderCost(){
		assertTrue(bookOrder.getBooks().size()==7);
	}
	
	
	

}
