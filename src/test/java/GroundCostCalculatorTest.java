import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Shipment.GrounIncrementCalculator;

public class GroundCostCalculatorTest {

	private GrounIncrementCalculator groundCostCalculator;

	@Before
	public void setup() {
		groundCostCalculator = new GrounIncrementCalculator();
	}

	@Test
	public void checkCalculateCost() {
		double basePrice = 10.00;
		assertEquals(0.50, groundCostCalculator.calculateCost(basePrice), .001);
	}

}
