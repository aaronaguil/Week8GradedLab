import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bobswarehouse.BookLookUpService;

import Exceptions.BookNotAvailableException;
import Shipment.ShipmentType;
import core.Book;
import core.HardCoverBook;
import core.Warehouse;
import core.TaxCalculator;

@RunWith(MockitoJUnitRunner.class)
public class WarehouseTest {
	
	
	Warehouse warehouse;

	@Mock
	BookLookUpService bookLookupService;
	
	@Mock
	TaxCalculator zipLookUpAdapter;
	
	@Before
	public void setup(){
		warehouse = new Warehouse(bookLookupService);
	}
	
	@Test
	public void test() throws BookNotAvailableException {
		Book book = new HardCoverBook("2388", 20.00, 75087L, ShipmentType.GROUND, zipLookUpAdapter);
		assertEquals(0, warehouse.retrieveAvailableQuantity(book));
	}

}
