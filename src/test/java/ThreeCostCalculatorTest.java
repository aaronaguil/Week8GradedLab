import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Shipment.ThreeDayIncrementCostCalculator;

public class ThreeCostCalculatorTest {

	private ThreeDayIncrementCostCalculator threeDayCostCalculator;
	
	@Before
	public void setup(){
		threeDayCostCalculator = new ThreeDayIncrementCostCalculator();
	}
	@Test
	public void checkCalculateCost() {
		double basePrice = 10.00; 
		assertEquals(1.50, threeDayCostCalculator.calculateCost(basePrice), .001);
	}

}
