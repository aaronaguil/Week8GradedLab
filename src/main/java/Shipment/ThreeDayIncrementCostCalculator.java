package Shipment;

public class ThreeDayIncrementCostCalculator implements IncrementCalculator{

	private double costFactor = .15;

	public double calculateCost(double price) {
		return price * costFactor;
	}
	
}
