package Shipment;

public class GrounIncrementCalculator implements IncrementCalculator{
	private double costFactor = .05;

	public double calculateCost(double price) {
		return price * costFactor;
	}

}
