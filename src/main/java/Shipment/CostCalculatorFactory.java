package Shipment;


import java.util.HashMap;
import java.util.Map;

import org.mockito.internal.matchers.Same;

import com.bobswarehouse.BookLookUpService;

public class CostCalculatorFactory {

	
	private Map<ShipmentType, IncrementCalculator> calculatorMap = new HashMap<ShipmentType, IncrementCalculator>();
	
	{
		calculatorMap.put(ShipmentType.GROUND, new GrounIncrementCalculator());
		calculatorMap.put(ShipmentType.SAME_DAY, new SameDayIncrementCalculator());
		calculatorMap.put(ShipmentType.THREE_DAY, new ThreeDayIncrementCostCalculator());
		
	}
	
	public IncrementCalculator getCostCalculator(ShipmentType shipmentType){
		return calculatorMap.get(shipmentType);
		
	}
	
}
