package Shipment;

public interface IncrementCalculator {

	double calculateCost(double price);
}
