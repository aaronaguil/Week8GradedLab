package Shipment;

public enum ShipmentType {

	
	SAME_DAY(24), THREE_DAY(72), GROUND(5);
	
	private int deliveryTimeHours;
	
	ShipmentType(int deliveryTimeHours){
		this.deliveryTimeHours = deliveryTimeHours;
	}
}
