package Shipment;

public class SameDayIncrementCalculator implements IncrementCalculator{

	
	private double costFactor = .20;

	
	public double calculateCost(double price) {
		return price * costFactor;
	}

}
