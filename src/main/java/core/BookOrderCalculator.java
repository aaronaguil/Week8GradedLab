package core;

import java.util.List;


import Exceptions.BookNotAvailableException;

public class BookOrderCalculator{

	
	private BookOrder bookOrder;
	private Warehouse warehouse;
	
	
	public BookOrderCalculator(BookOrder bookOrder, Warehouse warehouse) {
		this.bookOrder = bookOrder;
		this.warehouse = warehouse;
	}
	
	public double calculateOrderCost(){
		List<Book> books = bookOrder.getBooks();
		
		double cost = 0;
		for (Book book : books) {
			try{
				if(checkAvailability(book)){
					cost = cost + book.getCost();
				}
			}catch(BookNotAvailableException e){
				
			}
			
		}
		return Math.round(cost);
	}
	
	
	private boolean checkAvailability(Book book) throws BookNotAvailableException{
		int available = warehouse.retrieveAvailableQuantity(book);
		return available > 0;
	}
}
