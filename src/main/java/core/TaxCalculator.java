package core;

import com.roeriver.lookup.ZipCodeLookUp;

public class TaxCalculator {

	private ZipCodeLookUp zipCodeLookup;
	

	public TaxCalculator(ZipCodeLookUp zipCodeLookup) {
		this.setZipCodeLookup(zipCodeLookup);
	}

	public double lookUpZip(long zipCode){
		return zipCodeLookup.lookUpZip(zipCode);
	}

	public ZipCodeLookUp getZipCodeLookup() {
		return zipCodeLookup;
	}

	public void setZipCodeLookup(ZipCodeLookUp zipCodeLookup) {
		this.zipCodeLookup = zipCodeLookup;
	}
	
	
}
