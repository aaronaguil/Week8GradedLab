package core;

import com.roeriver.lookup.ZipCodeLookUp;

import Shipment.IncrementCalculator;
import Shipment.CostCalculatorFactory;
import Shipment.ShipmentType;

public class HardCoverBook extends Book{

	
	private ShipmentType shipmentType;
	
	public HardCoverBook(String isbnNumber, double basePrice, long zipCode, ShipmentType shipmentType, TaxCalculator zipLookUpAdapter) {
		super(isbnNumber, basePrice, zipCode, zipLookUpAdapter);
		this.setShipmentType(shipmentType);
		
		
	}

	public ShipmentType getShipmentType() {
		return shipmentType;
	}

	private void setShipmentType(ShipmentType shipmentType) {
		this.shipmentType = shipmentType;
	}
	
	
	public double getCost(){
		IncrementCalculator costCalculator = new CostCalculatorFactory().getCostCalculator(shipmentType);
		double shipmentIncrement = costCalculator.calculateCost(getBasePrice());
		double salesTax = gettaxCalculator().lookUpZip(getZipCode())/100 * getBasePrice();
		System.out.println(shipmentIncrement + getBasePrice() + salesTax);
		return Math.round(shipmentIncrement + getBasePrice() + salesTax);
	}
}
