package core;

public class DownloadableBook extends Book {

	
	public DownloadableBook(String isbnNumber, double basePrice, Long zipCode, TaxCalculator zipLookUpAdapter) {
		super(isbnNumber, basePrice, zipCode, zipLookUpAdapter);
	}

}
