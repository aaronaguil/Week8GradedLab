package core;
import java.util.ArrayList;
import java.util.List;

import Shipment.ShipmentType;

public class BookOrder {

	List<Book> books;
	

	public void addBook(Book book) {
		if (books == null) {
			this.books = new ArrayList<Book>();
		}
		this.books.add(book);
	}
	
	public List<Book> getBooks() {
		return books;
	}
	
}
