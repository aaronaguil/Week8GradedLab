package core;

import com.bobswarehouse.BookLookUpService;

import Exceptions.BookNotAvailableException;

public class Warehouse {

	private BookLookUpService bookLookUpService;

	public Warehouse(BookLookUpService bookLookUpService) {
		this.bookLookUpService = bookLookUpService;
	}
	
	public int retrieveAvailableQuantity(Book book) throws BookNotAvailableException{
		int availability = bookLookUpService.lookup(book.getIsbnNumber());
		if(availability < 1){
			throw new BookNotAvailableException();
		}
		return 1;
	}
}
