package core;

public class Book {

	
	private String isbnNumber;
	private double basePrice;
	private long zipCode;
	private TaxCalculator taxCalculator;
	
	Book(String isbnNumber, double basePrice, long zipCode, TaxCalculator taxCalculator){
		this.setIsbnNumber(isbnNumber);
		this.setBasePrice(basePrice);
		this.setZipCode(zipCode);
		this.taxCalculator = taxCalculator;
		
	}


	public String getIsbnNumber() {
		return isbnNumber;
	}



	public double getBasePrice() {
		return basePrice;
	}



	public long getZipCode() {
		return zipCode;
	}



	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}


	public void setZipCode(long zipCode) {
		this.zipCode = zipCode;
	}


	private void setIsbnNumber(String isbnNumber) {
		this.isbnNumber = isbnNumber;
	}
	
	
	public double getCost(){
		
		double salesTax = taxCalculator.lookUpZip(zipCode)/100;
		System.out.println(basePrice * (1+salesTax));
		return Math.round(basePrice * (1+salesTax));
	}


	public TaxCalculator gettaxCalculator() {
		return taxCalculator;
	}


	public void settaxCalculator(TaxCalculator taxCalculator) {
		this.taxCalculator = taxCalculator;
	}
	
	
}
