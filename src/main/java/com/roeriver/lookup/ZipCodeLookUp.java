package com.roeriver.lookup;

public interface ZipCodeLookUp {
	public double lookUpZip(long zipCode);
}
